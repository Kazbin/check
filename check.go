// Package check right now only consists of a little error checker
// Which I Kinda feel saves me some work.
// In the future i want to implement Panic Recovery
package check

import (
	"github.com/fatih/color"
	"os"
)

// func Err(err error) takes an error as an argument and prints
// it in red text to the error Console and then exits
// the Application with the Exit-Code 1
func Err(err error) {
	if err != nil {
		// Create a custom print function for convenient
		red := color.New(color.FgRed).PrintfFunc()
		red("Error:\" %s\"\n", err.Error())
		os.Exit(1)
	}
}
